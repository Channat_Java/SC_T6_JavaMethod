package org.kshrd.my_package2;

import org.kshrd.my_package1.MyClass;

public class MyClass2 extends MyClass{

    public static void main(String[] args) {
        MyClass myClass = new MyClass();
        myClass.publicVar = "";
        MyClass2 myClass2 = new MyClass2();
        myClass2.protectedVar = 34;

    }
}
