package org.kshrd.my_package1;

public class MyClass {

    private int privateVar;
    int defaultVar;
    public String publicVar;
    protected int protectedVar;

    public static void main(String[] args) {
        MyClass myClass = new MyClass();
        myClass.privateVar = 0;
    }

}
