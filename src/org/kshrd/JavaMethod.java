package org.kshrd;

public class JavaMethod {



    /*
        modifier returnType methodName([Parameter List]) {

            // Statements
            [return returnTypeValue];
        }
     */

    private void printMessage() {
        // statement
        System.out.println("Hello world.");
    }
    // Method print student name, for whatever user input.

    void printStudentName(String name) {
        System.out.println("Student name: " + name);
    }


    public static void main(String[] args) {
	    JavaMethod javaMethod = new JavaMethod();
	    javaMethod.printMessage();
        String n = "Mr. Boly";
	    javaMethod.printStudentName(n);
    }
}
